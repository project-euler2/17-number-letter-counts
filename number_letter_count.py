from num2words import num2words

def sum_words(start,end):
    sum = 0
    for i in range(start,end+1):
        sum += int(len((num2words(i).replace('-','')).replace(' ','')))
    return sum

print(sum_words(1,1000))